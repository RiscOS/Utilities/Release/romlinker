/* Copyright 2001 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * ROM Linker
 *
 * Copyright (C) Pace Micro Technology plc. 2001
 *
 */
#ifndef memory_h_included
#define memory_h_included

/* Allocate memory from heap - do not return if allocation failed */
extern void *safe_alloc(const unsigned long amount);

/* Allocate memory from heap - do not return if allocation failed */
extern void *safe_realloc(void *ptr, const unsigned long amount);

/* Duplicate a string, s1 == NULL => return NULL, memory alloc fail => NULL
 * To fault NULL copies and memory failures, wrap with faultnull().
 */
extern char *Strdup(const char *s1);

/* Returns p if p is non-NULL, calls err_fail(error) if p is NULL */
#ifdef __CC_NORCROFT
#  pragma -v1
#endif
extern void *fault_null(void *p, const char *error, ...);
extern const void *fault_null_const(const void *p, const char *error, ...);
#ifdef __CC_NORCROFT
#  pragma -v0
#endif

#endif
