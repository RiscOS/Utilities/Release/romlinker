/* Copyright 2001 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * ROM Linker
 *
 * Copyright (C) Pace Micro Technology plc. 2001
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef RISCOS
#  include "kernel.h"
#  include "swis.h"
#  include "Global/FileTypes.h"
#endif

/* CLX headers */
#include "err.h"

/* Local headers */
#include "memory.h"
#include "romlinker.h"
#include "filewriter.h"

/* Opens the image file for writing.  If possible, it will arrange for the
 * entire file to be buffered in memory.  This will assist performance
 * enormously if this is possible.
 */
FILE *filewriter_image_open(const char *name, unsigned long size)
{
  FILE *f = fault_null(fopen(name, "wb"), "Unable to open image file '%s'", name);

  if (setvbuf(f, NULL, _IOFBF, (size_t) size)) {
    /* Not buffering the file */
    err_report("insufficient memory to cache ROM image - using paging");
  }

  return f;
}

/* Sets the filetype of the output file if executing on RISC OS; does nothing
 * otherwise
 */
void filewriter_mark_as_rom_image(const char *name)
{
#  ifdef RISCOS
   (void) _swix(OS_File, _INR(0,2), 18, name, FileType_EPROM);
#  else
   (void) name;
#  endif
}
